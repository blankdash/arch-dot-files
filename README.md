# Arch Dot Files

This contains all of my important Arch linux Dot files, Feel free to use them.

Rename the following files & folders 

zshrc       --> .zshrc
xresources  --> .xresources
oh-my-shell --> .oh-my-shell

The below lines are added to my zshrc file to display color scripts everytime you open a zsh shell, that can be added to bashrc file too.
Inorder for this to work, you should have .oh-myshell, folder in your home directory containing the color scripts.
you can modify the code below if you like to.

for j in {0}
	do
		i=$(echo $((1 + RANDOM % 14)));
		if
				[ -s ~/.oh-my-shell/$i ];
			then
    				source ~/.oh-my-shell/$i;
		fi
	done
